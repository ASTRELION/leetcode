#!/bin/bash

if [[ $# == 0 ]]; then
    echo "Running all solutions..."

    for file in ./solutions/*
    do
        if [[ $file == *.cpp ]]; then
            echo
            echo "Compiling $file..."
            g++ $file -o ./solutions/a.out
            echo "Running $file..."
            ./solutions/a.out
        fi
    done
else
    echo "Compiling problem$1.cpp..."
    g++ ./solutions/problem$1.cpp -o ./solutions/a.out
    echo "Running problem$1.cpp..."
    ./solutions/a.out
fi