// LeetCode - Problem 495 - Teemo Attacking
// https://leetcode.com/problems/teemo-attacking/

#include <iostream>
#include <vector>

class Solution
{
    public:
    int findPoisonedDuration(std::vector<int>& timeSeries, int duration)
    {
        int timePoisoned = 0;

        for (int i = 0; i < timeSeries.size() - 1; i++)
        {
            timePoisoned += std::min(timeSeries[i + 1] - timeSeries[i], duration);
        }

        return timePoisoned + duration;
    }
};

int main()
{
    Solution solution;
    std::vector<int> timeSeries{ 1, 4 };
    int result = solution.findPoisonedDuration(timeSeries, 2);

    std::cout << "Solution 495:" << std::endl;
    std::cout << result << std::endl;

    return 0;
}