// LeetCode - Problem 1672 - Richest Customer Wealth
// https://leetcode.com/problems/richest-customer-wealth/

#include <iostream>
#include <vector>

class Solution
{
    public:
    int maximumWealth(std::vector<std::vector<int>>& accounts)
    {
        int maxWealth = 0;
        
        for (int i = 0; i < accounts.size(); i++)
        {
            int wealth = 0;
            
            for (int w = 0; w < accounts[i].size(); w++)
            {
                wealth += accounts[i][w];
            }
            
            if (wealth > maxWealth)
            {
                maxWealth = wealth;
            }
        }
        
        return maxWealth;
    }
};

int main()
{
    Solution solution;
    
    std::vector<std::vector<int>> input = {{ 1, 2, 3 }, {3, 2, 1}};
    
    int result = solution.maximumWealth(input);
    
    std::cout << "Solution 1672:" << std::endl;
    std::cout << result << std::endl;
    
    return 0;
}