// LeetCode - Problem 421 - Maximum XOR of Two Numbers in an Array
// https://leetcode.com/problems/maximum-xor-of-two-numbers-in-an-array/

#include <iostream>
#include <vector>

class Solution
{
    public:
    int findMaximumXOR(std::vector<int>& nums)
    {
        int max = 0;

        for (int i = 0; i < nums.size(); i++)
        {
            for (int j = i; j < nums.size(); j++)
            {
                int xorNum = nums[i] ^ nums[j];

                if (xorNum > max)
                {
                    max = xorNum;
                }
            }
        }

        return max;
    }
};

int main()
{
    Solution solution;

    std::vector<int> nums{ 3, 10, 5, 25, 2, 8 };
    int result = solution.findMaximumXOR(nums);

    std::cout << "Soluton 421:" << std::endl;
    std::cout << result << std::endl;

    return 0;
}