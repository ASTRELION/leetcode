// LeetCode - Problem 142 - Linked List Cycle II
// https://leetcode.com/problems/linked-list-cycle-ii/

#include <iostream>
#include <unordered_set>

/**
 * Represents a simple linked graph node
 */
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

/**
 * Solution 1 - Intuitive solution.
 *
 * Traverse the graph and add each node to a hash-set. For each node, check
 * if it already exists in the hash-set. If it does, return its next pointer.
 * If we never return in the while loop, no cycle was found, so return NULL.
 *
 * Runtime: O(n)
 * Memory: O(n)
 */
class Solution1
{
    public:
    ListNode* detectCycle(ListNode *head)
    {
        std::unordered_set<ListNode*> nodes;

        ListNode *current = head;
        while (current && current->next)
        {
            nodes.insert(current);
            
            // found cycle
            if (nodes.find(current->next) != nodes.end())
            {
                return current->next;
            }

            current = current->next;
        }

        return NULL;
    }
};

/**
 * Solution 2 - Floyd's cycle algorithm.
 *
 * https://leetcode.com/problems/linked-list-cycle-ii/discuss/1701055/JavaC%2B%2BPython-best-explanation-ever-happen's-for-this-problem.
 * Maintain a 'slow' and a 'fast' node pointer. Increment the slow pointer by one, and the fast pointer
 * by two. If both points point to the same node, then a cycle exists.
 *
 * Runtime: O(n)
 * Memory: O(1)
 */
class Solution2
{
    public:
    ListNode* detectCycle(ListNode *head)
    {
        std::unordered_set<ListNode*> nodes;

        ListNode* slow = head;
        ListNode* fast = head;

        while (fast && fast->next)
        {
            slow = slow->next;
            fast = fast->next->next;

            // found cycle
            if (slow == fast)
            {
                slow = head;
                while (slow != fast)
                {
                    slow = slow->next;
                    fast = fast->next;
                }

                return slow;
            }
        }

        return NULL;
    }
};

int main()
{
    ListNode node0 = { 0 };
    ListNode node1 = { 1 };
    ListNode node2 = { 2 };
    ListNode node3 = { 3 };

    node0.next = &node1;
    node1.next = &node2;
    node2.next = &node3;
    node3.next = &node0;

    Solution2 solution;

    std::cout << "Solution 142:" << std::endl;
    std::cout << solution.detectCycle(&node0)->val << std::endl;
    
    return 0;
}