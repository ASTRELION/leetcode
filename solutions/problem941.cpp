// LeetCode - Problem 941 - Valid Mountain Array
// https://leetcode.com/problems/valid-mountain-array/

#include <iostream>
#include <vector>

class Solution
{
    public:
    bool validMountainArray(std::vector<int>& arr)
    {
        if (arr.size() < 3)
        {
            return false;
        }

        int i = 0;

        while (i + 1 < arr.size() && arr[i] < arr[i + 1])
        {
            i++;
        }

        if (i == 0 || i == arr.size() - 1)
        {
            return false;
        }

        while (i + 1 < arr.size() && arr[i] > arr[i + 1])
        {
            i++;
        }
        
        return i == arr.size() - 1;
    }
};

int main()
{
    Solution solution;

    std::vector<int> input1{ 2, 1 };
    std::vector<int> input2{ 3, 5, 5 };
    std::vector<int> input3{ 0, 3, 2, 1 };

    std::cout << "Solution 941:" << std::endl;
    std::cout << solution.validMountainArray(input1) << std::endl;
    std::cout << solution.validMountainArray(input2) << std::endl;
    std::cout << solution.validMountainArray(input3) << std::endl;

    return 0;
}