// LeetCode - Problem 1 - Two Sum
// https://leetcode.com/problems/two-sum/

#include <iostream>
#include <vector>
#include <unordered_map>

class Solution
{
    public:
    std::vector<int> twoSum(std::vector<int>& nums, int target)
    {
        std::unordered_map<int, int> used;

        for (int i = 0; i < nums.size(); i++)
        {
            auto search = used.find(target - nums[i]);

            if (search != used.end())
            {
                return std::vector<int>{ search->second, i };
            }
            
            used[nums[i]] = i;
        }

        return std::vector<int>();
    }
};

int main()
{
    Solution solution;

    std::vector<int> nums1{ 2, 7, 11, 15 };
    std::vector<int> nums2{ 3, 2, 4 };
    std::vector<int> nums3{ 3, 3 };

    int target1 = 9;
    int target2 = 6;
    int target3 = 6;

    std::vector<int> result1 = solution.twoSum(nums1, target1);
    std::vector<int> result2 = solution.twoSum(nums2, target2);
    std::vector<int> result3 = solution.twoSum(nums3, target3);

    std::cout << "Solution 1:" << std::endl;
    std::cout << result1[0] << " " << result1[1] << std::endl;
    std::cout << result2[0] << " " << result2[1] << std::endl;
    std::cout << result3[0] << " " << result3[1] << std::endl;

    return 0;
}