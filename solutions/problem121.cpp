// LeetCode - Problem 121 - Best Time to Buy and Sell Stock
// https://leetcode.com/problems/best-time-to-buy-and-sell-stock/

#include <cstdint>
#include <iostream>
#include <vector>
#include <climits>

class Solution
{
    public:
    int maxProfit(std::vector<int>& prices)
    {
        int maxProfit = 0, minPrice = prices[0];
        
        for (int price : prices)
        {
            minPrice = std::min(minPrice, price);
            int profit = price - minPrice;
            maxProfit = std::max(maxProfit, profit);
        }
        
        return maxProfit;
    }
};

int main()
{
    Solution solution;
    
    std::vector<int> input = { 7, 1, 5, 3, 6, 4 };
    
    std::cout << "Solution 121:" << std::endl;
    std::cout << solution.maxProfit(input) << std::endl;
    
    return 0;
}