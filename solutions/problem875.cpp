// LeetCode - Problem 875 - Koko Eating Bananas
// https://leetcode.com/problems/koko-eating-bananas/

#include <cmath>
#include <iostream>
#include <vector>

class Solution
{
    public:
    int minEatingSpeed(std::vector<int>& piles, int h)
    {
        int len = piles.size();

        int max = piles[0];
        for (int i = 1; i < len; i++)
        {
            if (max < piles[i])
            {
                max = piles[i];
            }
        }

        if (h <= len || len == 0)
        {
            return max;
        }

        if (len == 1)
        {
            return std::ceil((double)piles[0] / (double)h);
        }

        int left = 1;
        int right = max;

        // binary search
        while (left < right)
        {
            int mid = (left + right) / 2;

            // check if Koko can eat all bananas
            if (canEatPiles(piles, h, mid))
            {
                right = mid;
            }
            else 
            {
                left = mid + 1;
            }
        }

        return right;
    }

    bool canEatPiles(std::vector<int>& piles, int h, int k)
    {
        int j = 0; // hours spent so far eating
        for (int i : piles)
        {
            j += std::ceil((double)i / (double)k);

            if (j > h)
            {
                return false;
            }
        }

        return true;
    }
};

int main()
{
    Solution solution;
    std::vector<int> piles{ 30, 11, 23, 4, 20 };
    int h = 5;

    int result = solution.minEatingSpeed(piles, h);

    std::cout << "Solution 875:" << std::endl;
    std::cout << result << std::endl;

    return 0;
}