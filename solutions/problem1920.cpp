// LeetCode - Problem 1920 - Build Array from Permutation
// https://leetcode.com/problems/build-array-from-permutation/

#include <iostream>
#include <vector>

class Solution
{
    public:
    std::vector<int> buildArray(std::vector<int>& nums)
    {
        std::vector<int> ans(nums.size());

        for (int n : nums)
        {
            ans[n] = nums[nums[n]];
        }

        return ans;
    }
};

int main()
{
    Solution soluton;

    std::vector<int> input1{ 0, 2, 1, 5, 3, 4 };
    std::vector<int> result1 = soluton.buildArray(input1);

    std::cout << "Soluton 1920:" << std::endl;

    for (int i : result1)
    {
        std::cout << i << " ";
    }

    std::cout << std::endl;

    return 0;
}