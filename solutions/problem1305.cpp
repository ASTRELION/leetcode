// LeetCode - Problem 1305 - All Elements in Two Binary Search Trees
// https://leetcode.com/problems/all-elements-in-two-binary-search-trees/

#include <iostream>
#include <vector>

struct TreeNode
{
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(NULL), right(NULL) {}
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Solution
{
    public:
    std::vector<int> getAllElements(TreeNode* root1, TreeNode* root2)
    {
        std::vector<int> left;
        std::vector<int> right;

        inOrder(root1, left);
        inOrder(root2, right);

        int i = 0, j = 0, k = 0;
        std::vector<int> ascending(left.size() + right.size());

        while (i < left.size() && j < right.size())
        {
            if (left[i] < right[j])
            {
                ascending[k++] = left[i++];
            }
            else
            {
                ascending[k++] = right[j++];
            }
        }

        while (i < left.size())
        {
            ascending[k++] = left[i++];
        }

        while (j < right.size())
        {
            ascending[k++] = right[j++];
        }

        return ascending;
    }

    void inOrder(TreeNode* root, std::vector<int>& order)
    {
        if (root)
        {
            inOrder(root->left, order);
            order.push_back(root->val);
            inOrder(root->right, order);
        }
    }
};

int main()
{
    Solution solution;

    TreeNode root12(1);
    TreeNode root13(4);
    TreeNode root11(2, &root12, &root13);

    TreeNode root22(0);
    TreeNode root23(3);
    TreeNode root21(1, &root22, &root23);

    std::vector<int> result = solution.getAllElements(&root11, &root21);

    std::cout << "Solution 1305:" << std::endl;

    for (int n : result)
    {
        std::cout << n << " ";
    }

    std::cout << std::endl;

    return 0;
}