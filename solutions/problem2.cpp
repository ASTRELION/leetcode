// LeetCode - Problem 2 - Add Two Numbers
// https://leetcode.com/problems/add-two-numbers/

#include <iostream>
#include <cmath>

struct ListNode
{
    int val;
    ListNode *next;
    ListNode() : val(0), next(NULL) {}
    ListNode(int x) : val(x), next(NULL) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution
{
    public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2)
    {
        ListNode sum;
        ListNode* current = &sum;

        int carry = 0;
        while (l1 || l2 || carry)
        {
            int tempSum = carry;
            if (l1) tempSum += l1->val;
            if (l2) tempSum += l2->val;

            carry = tempSum / 10;

            current->next = new ListNode(tempSum % 10);
            current = current->next;
            l1 = l1 ? l1->next : l1;
            l2 = l2 ? l2->next : l2;
        }

        return sum.next;
    }
};

int main()
{
    Solution solution;

    ListNode l1n3(3);
    ListNode l1n2(4, &l1n3);
    ListNode l1n1(2, &l1n2);
    ListNode l2n3(4);
    ListNode l2n2(6, &l2n3);
    ListNode l2n1(5, &l2n2);

    ListNode* result = solution.addTwoNumbers(&l1n1, &l2n1);

    std::cout << "Solution 2:" << std::endl;

    ListNode* current = result;
    while (current)
    {
        std::cout << current->val;
        current = current->next;
    }

    std::cout << std::endl;

    return 0;
}