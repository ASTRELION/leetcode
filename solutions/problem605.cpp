// LeetCode - Problem 605 - Can Place Flowers
// https://leetcode.com/problems/can-place-flowers/

#include <iostream>
#include <ratio>
#include <vector>
#include <chrono>

/**
 * Solution 1.
 *
 * Iterate over the flower bed, if we are at a 0 and the previous and next are 0's,
 * we plant. When we plant we can skip the next plot since it will never be valid.
 */
class Solution
{
    public:
    bool canPlaceFlowers(std::vector<int>& flowerbed, int n)
    {
        if (n == 0)
        {
            return true;
        }

        for (int i = 0; i < flowerbed.size(); i++)
        {
            if (flowerbed.at(i) == 0 && 
                (i == 0 || flowerbed.at(i - 1) == 0) && 
                (i == flowerbed.size() - 1 || flowerbed.at(i + 1) == 0))
            {
                n--;
                i++;

                if (n == 0) return true;
            }
        }

        return false;
    }
};

int main()
{
    Solution solution;
    std::vector<int> flowerbed{1, 0, 0, 0, 0, 0, 1};

    bool result = solution.canPlaceFlowers(flowerbed, 2);

    std::cout << "Solution 605:" << std::endl;
    std::cout << result << std::endl;

    return 0;
}