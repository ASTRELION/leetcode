# [LeetCode](https://leetcode.com/problemset/all/) Solutions

## [Finished Solutions](./solutions/)

| Problem | Solution |
|---------|----------|
| [Problem 1 - Two Sum][problem1]                                       | [`problem1.cpp`](./solutions/problem1.cpp) |
| [Problem 2 - Add Two Numbers][problem2]                               | [`problem2.cpp`](./solutions/problem2.cpp) |
| [Problem 121 - Best Time to Buy and Sell Stock][problem121]           | [`problem121.cpp`](./solutions/problem121.cpp) |
| [Problem 142 - Linked List Cycle II][problem142]                      | [`problem142.cpp`](./solutions/problem142.cpp) |
| [Problem 421 - Maximum XOR of Two Numbers in an Array][problem421]    | [`problem421.cpp`](./solutions/problem421.cpp) |
| [Problem 495 - Teemo Attacking][problem495]                           | [`problem495.cpp`](./solutions/problem495.cpp) |
| [Problem 605 - Can Place Flowers][problem605]                         | [`problem605.cpp`](./solutions/problem605.cpp) |
| [Problem 875 - Koko Eating Bananas][problem875]                       | [`problem875.cpp`](./solutions/problem875.cpp) |
| [Problem 941 - Valid Mountain Array][problem941]                      | [`problem941.cpp`](./solutions/problem941.cpp) |
| [Problem 1305 - All Elements in Two Binary Search Trees][problem1305] | [`problem1305.cpp`](./solutions/problem1305.cpp) |
| [Problem 1672 - Richest Customer Wealth][problem1672]                 | [`problem1672.cpp`](./solutions/problem1672.cpp) |
| [Problem 1920 - Build Array from Permutation][problem1920]            | [`problem1920.cpp`](./solutions/problem1920.cpp) |

*All code inside the `main()` function is generally not part of the solution and is largely used explicity for 
testing/examples. Problem specific code is in the `Solution` class, usually in the first function.*

## Running a Solution

Run `./run.sh` to compile and run every solution in [`solutions/`](./solutions/).  
Run `./run.sh <number>` to run a specific solution, e.g. `./run.sh 941`.

Otherwise, on Linux, you can compile a problem with `g++ <filename>` and run it with `./a.out`.

[problem1]:     https://leetcode.com/problems/two-sum/
[problem2]:     https://leetcode.com/problems/add-two-numbers/
[problem121]:   https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
[problem142]:   https://leetcode.com/problems/linked-list-cycle-ii/
[problem421]:   https://leetcode.com/problems/maximum-xor-of-two-numbers-in-an-array/
[problem495]:   https://leetcode.com/problems/teemo-attacking/
[problem605]:   https://leetcode.com/problems/can-place-flowers/
[problem875]:   https://leetcode.com/problems/koko-eating-bananas/
[problem941]:   https://leetcode.com/problems/valid-mountain-array/
[problem1305]:  https://leetcode.com/problems/all-elements-in-two-binary-search-trees/
[problem1672]:  https://leetcode.com/problems/richest-customer-wealth/
[problem1920]:  https://leetcode.com/problems/build-array-from-permutation/